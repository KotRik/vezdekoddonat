import React from 'react';
import bridge from '@vkontakte/vk-bridge';
import Banner from '@vkontakte/vkui/dist/components/Banner/Banner';
import Button from '@vkontakte/vkui/dist/components/Button/Button';
import FormLayout from '@vkontakte/vkui/dist/components/FormLayout/FormLayout';
import Input from '@vkontakte/vkui/dist/components/Input/Input';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import PanelHeaderButton from '@vkontakte/vkui/dist/components/PanelHeaderButton/PanelHeaderButton';
import Select from '@vkontakte/vkui/dist/components/Select/Select';

import Icon28Calendar from '@vkontakte/icons/dist/28/calendar_outline';
import Icon28ChevronBack from '@vkontakte/icons/dist/28/chevron_back';
import Icon28Picture from '@vkontakte/icons/dist/28/picture_outline';
import Icon28Target from '@vkontakte/icons/dist/28/target_outline';

import '@vkontakte/vkui/dist/vkui.css';
import './App.scss';

import { Stack, Page } from "vkui-navigator/dist";
import { Checkbox, FormLayoutGroup, Radio, Textarea } from '@vkontakte/vkui';

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			radioType: 0
		}
	}


	componentDidMount() {
		bridge.subscribe(({ detail: { type, data }}) => {
			if (type === 'VKWebAppUpdateConfig') {
				const schemeAttribute = document.createAttribute('scheme');
				schemeAttribute.value = data.scheme ? data.scheme : 'client_light';
				document.body.attributes.setNamedItem(schemeAttribute);
			}
		});

		/*
			async function fetchData() {
				const user = await bridge.send('VKWebAppGetUserInfo');
				setUser(user);
				setPopout(null);
			}
			fetchData();
		*/
	}

	Panel1({ id, navigator }) {
		return (
			<Panel id={id}>
				<PanelHeader>
					Пожертвования 
				</PanelHeader>

				<div class="App__center">
					<p className="App__text">У Вас пока нет сборов.<br/>
					Начните доброе дело.</p>
					<Button onClick={(e)=>navigator.go("panel2")} align="center" mode="primary">Создать сбор</Button>	
				</div>	
			</Panel>
		)
	}

	Panel2({ id, navigator }) {
		return (
			<Panel id={id}>
				<PanelHeader
					left={<PanelHeaderButton onClick={(e)=>navigator.go("panel1")}><Icon28ChevronBack/></PanelHeaderButton>}>
					Тип сбора
				</PanelHeader>
				
				<div style={{flex: '1', justifyContent: 'center'}}>
					<Banner
						before={<Icon28Target/>}
						header="Целевой сбор"
						subheader="Когда есть определённая цель"
						asideMode="expand"
						onClick={(e)=>navigator.go("panel3")}
					/>
					<Banner
						before={<Icon28Calendar/>}
						header="Регулярный сбор"
						subheader="Если помощь нужна ежемесячно"
						asideMode="expand"
						onClick={(e)=>navigator.go("panel4")}
					/>
				</div>
			</Panel>
		)
	}	

	Panel3 = ({ id, navigator }) => {
		return (
			<Panel id={id}>
				<PanelHeader
					left={<PanelHeaderButton onClick={(e)=>navigator.go("panel2")}><Icon28ChevronBack/></PanelHeaderButton>}>
					Целевой сбор
				</PanelHeader>
				<FormLayout>
					<div className="App__upload">
						<Icon28Picture/> <p className="App__text--upload">Загрузить обложку</p>
					</div>
					<Input  top="Название сбора" placeholder="Название сбора"/>

					<Input  top="Сумма, ₽" placeholder="Сколько нужно собрать?" />

					<Input  top="Цель" placeholder="Например, лечение человека" />

					<Textarea  top="Описание" placeholder="На что пойдут деньги и как они кому-то помогут?" />

					<Select  top="Куда получать деньги" placeholder="Выберите из списка...">
						<option value="1">Счёт VK Pay - 1234</option>
					</Select>

					<Button size="xl" onClick={(e)=>navigator.go("panel31")}>Далее</Button>
				</FormLayout>
			</Panel>
		)
	}

	Panel4 = ({ id, navigator }) => {
		return (
			<Panel id={id}>
				<PanelHeader
					left={<PanelHeaderButton onClick={(e)=>navigator.go("panel2")}><Icon28ChevronBack/></PanelHeaderButton>}>
					Регулярный сбор
				</PanelHeader>
				<FormLayout>
					<div className="App__upload">
						<Icon28Picture/> <p className="App__text--upload">Загрузить обложку</p>
					</div>
					<Input  type="number" top="Название сбора" placeholder="Название сбора"/>

					<Input  top="Сумма в месяц, ₽" placeholder="Сколько нужно в месяц?" />

					<Input  top="Цель" placeholder="Например, поддержка приюта" />

					<Textarea  top="Описание" placeholder="На что пойдут деньги и как они кому-то помогут?" />

					<Select  top="Куда получать деньги" placeholder="Выберите из списка...">
						<option value="1">Счёт VK Pay - 1234</option>
					</Select>

					<Select  top="Автор" placeholder="Выберите из списка...">
						<option value="1">Матвей Правосудов</option>
					</Select>

					<Button size="xl">Далее</Button>
				</FormLayout>
			</Panel>
		)
	}

	Panel31 = ({ id, navigator }) => {
		return (
			<Panel id={id}>
				<PanelHeader
					left={<PanelHeaderButton onClick={(e)=>navigator.go("panel2")}><Icon28ChevronBack/></PanelHeaderButton>}>
					Дополнительно
				</PanelHeader>
				<FormLayout>
					<Select  top="Автор" placeholder="Выберите из списка...">
						<option value="1">Матвей Правосудов</option>
					</Select>

					<FormLayoutGroup>
						<Radio description="Когда соберём сумму" onClick={(e)=>this.setState({radioType:0})} checked={this.state.radioType === 0}/> 
						<Radio description="В определённую дату" onClick={(e)=>this.setState({radioType:1})} checked={this.state.radioType === 1}/>
					</FormLayoutGroup>

					{
						(this.state.radioType === 1) ?
						<Select  top="Дата окончания" placeholder="Выберите...">
							<option value="1">20 сентября</option>
						</Select> : null
					}
					
					<Button size="xl" onClick={(e)=>{}}>Создать сбор</Button>
				</FormLayout>
			</Panel>
		)
	}

	render() {
		return (
			<Stack activePage="page1">
				<Page id="page1" activePanel="panel1">
					<this.Panel1 id="panel1"/>
					<this.Panel2 id="panel2"/>

					<this.Panel3 id="panel3"/>
					<this.Panel31 id="panel31"/>
					
					<this.Panel4 id="panel4"/>
				</Page>
			</Stack>
		)
	}

}

export default App;

